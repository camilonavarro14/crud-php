<nav class="navbar navbar-expand navbar-light navbar-bg">
    <a class="sidebar-toggle js-sidebar-toggle">
        <i class="hamburger align-self-center"></i>
    </a>

    <div class="navbar-collapse collapse">
        <ul class="navbar-nav navbar-align">
            <li class="nav-item dropdown">
                <a class="nav-icon dropdown-toggle" href="#" id="alertsDropdown" data-bs-toggle="dropdown">
                    <div class="position-relative">
                        <i class="align-middle" data-feather="bell"></i>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end py-0"
                     aria-labelledby="alertsDropdown">
                    <div style="height: 350px; overflow-y: scroll;">
                        <div class="list-group">
                            <?php
                            if (isset($_SESSION['notifications'])) {
                                for ($i=sizeof($_SESSION['notifications'])-1; $i >= 0 ; $i--) {
                                    ?>
                                    <a href="#" class="list-group-item">
                                        <div class="row g-0 align-items-center">
                                            <div class="col-2">
                                                <i class="text-warning" data-feather="bell"></i>
                                            </div>
                                            <div class="col-10">
                                                <div class="text-muted small mt-1">
                                                    <?php echo $_SESSION['notifications'][$i]?></div>
                                            </div>
                                        </div>
                                    </a>
                                    <?php
                                }
                            }
                            ?>

                        </div>
                    </div>
                    <div class="dropdown-menu-footer">
                        <a href="#" class="text-muted">Mostrar todas las notificaciones</a>
                    </div>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#" data-bs-toggle="dropdown">
                    <i class="align-middle" data-feather="settings"></i>
                </a>

                <a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#" data-bs-toggle="dropdown">
                    <img src="  https://image.flaticon.com/icons/png/512/16/16363.png"


                         class="avatar img-fluid rounded me-1" alt="Charles Hall" /> <span
                            class="text-dark"><?php echo $_SESSION['user_info']['NomUsuario']?></span>
                </a>
                <div class="dropdown-menu dropdown-menu-end">
                    <a class="dropdown-item" href="../app/logout.php">Cerrar sesión</a>
                </div>
            </li>
        </ul>
    </div>
</nav>