<nav id="sidebar" class="sidebar js-sidebar">
    <div class="sidebar-content js-simplebar">
        <a class="sidebar-brand" href="./admin.php">
            <span class="align-middle">FACTURACIÓN E INVENTARIOS</span>
        </a>
        <ul class="sidebar-nav">
            <li class="sidebar-header">
            </li>
            <li class="sidebar-item ">
                <a class="sidebar-link" href="./adminRoles.php">
                    <i class="align-middle" data-feather="sliders"></i> <span
                            class="align-middle">Roles</span>
                </a>
            </li>
            <li class="sidebar-item ">
                <a class="sidebar-link" href="./adminUsuario.php">
                    <i class="align-middle" data-feather="sliders"></i> <span
                            class="align-middle">Usuarios</span>
                </a>
            </li>
            <li class="sidebar-item ">
                <a class="sidebar-link" href="./adminProductos.php">
                    <i class="align-middle" data-feather="sliders"></i> <span
                            class="align-middle">Productos</span>
                </a>
            </li>
        </ul>

    </div>
</nav>