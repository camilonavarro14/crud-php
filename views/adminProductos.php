<?php
    require_once "../functions.php";
    session_start();
    
    if(isset($_SESSION['logued']) && $_SESSION['logued'] == true ){
        require_once "../database.php";
        ?>
<?php include_once "plantilla/body.php"?>
    <div class="wrapper">
        <?php include_once "plantilla/menu.php"?>

        <div class="main">
            <?php include_once "plantilla/titulo.php"?>

            <main class="content">
                <div class="container-fluid p-0">
                    <div class="row mb-4 px-3">
                        <?php
                                    if (isset($_SESSION['message']) && isset($_SESSION['type'])) {
                                        ?>
                        <div class="alert alert-<?php echo $_SESSION['type']?> alert-dismissible" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <div class="alert-message">
                                <?php echo $_SESSION['message']?>
                            </div>
                        </div>
                        <?php
                                    unset($_SESSION['message']);
                                    unset($_SESSION['type']);
                                    }

                                ?>
                    </div>

                    <button type="button" class="sidebar-link" data-bs-toggle="modal"
                            data-bs-target="#creatingCenteredModalPrimary">
                        <i class="fa fa-eyedropper" aria-hidden="true"></i><span class="align-middle">Insertar
                                Rol</span>
                    </button>
                    <div class="modal fade" id="creatingCenteredModalPrimary" tabindex="-2" role="dialog" aria-hidden="true">
                        <form action="../app/insertarRol.php" method="POST">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Insertar Rol</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body m-3">
                                        <div class="mb-3">
                                            <label class="form-label">Nombre</label>
                                            <input type="text" class="form-control" name="NomRole" placeholder="Nombre"
                                                   required>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Descripcion</label>
                                            <input type="text" class="form-control" name="DesRole"
                                                   placeholder="Descripcion" required>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                                        <button type="submit" class="btn btn-primary">Insertar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-12 col-xxl-12 d-flex">
                            <div class="card flex-fill">
                                <div class="card-header">
                                    <h5 class="card-title mb-0">Todos los Usuarios</h5>
                                </div>

                                <table class="table table-hover my-0">
                                    <thead>
                                        <tr>
                                            <th>Email</th>
                                            <th>Nombre</th>
                                            <th>Accion</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $sql = "SELECT * FROM usuario where estado = 1";
                                    $result = mysqli_query($con,$sql);
                                    while ($rol = $result->fetch_assoc()) { ?>
                                        <tr>
                                            <td><?php echo $rol['Email']?></td>
                                            <td><?php echo $rol['NomUsuario']?></td>
                                            <td>
                                                <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                                        data-bs-target="#centeredModalPrimary<?php echo $rol['IdUsuario']?>">
                                                    <i class="fa fa-eyedropper" aria-hidden="true"></i>
                                                </button>



                                            </td>
                                        </tr>
                                    <?php  } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
        <script src="js/app.js"></script>
        <?php include "./ partials/footer.php"?>
        </body>

        </html>
<?php
    }else{
        header("Location: ../index.php");
    }
?>