<?php
    require_once "../functions.php";
    session_start();

    $accion = $_POST['Accion'];
    $ruta ="Location: ../views/adminRoles.php";

    if($accion === 'created'){
        $name = $_POST['NomRole'];
        $description = $_POST['DesRole'];
        $created = createRol($name,$description);

        if ($created) {
            $_SESSION['message'] = "Rol creado correctamente...";
            $_SESSION['type'] = "success";
            $fecha = new DateTime();
            $timestamp = $fecha->format('Y-m-d H:i:s') ;
            array_push($_SESSION['notifications'],"Rol creado correctamente el ".$timestamp);
            header($ruta);
        }else{
            $_SESSION['message'] = "Error al crear el Rol.";
            $_SESSION['type'] = "danger";
            $fecha = new DateTime();
            $timestamp = $fecha->format('Y-m-d H:i:s') ;
            array_push($_SESSION['notifications'],"Error al crear Rol el ".$timestamp);
            header($ruta);
        }
    }

    if($accion === 'updated'){
        $id = $_POST['idRole'];
        $name = $_POST['NomRole'];
        $description = $_POST['DesRole'];
        $updated = updateRol($id,$name,$description);
        if ($updated) {
            $_SESSION['message'] = "Rol editado correctamente...";
            $_SESSION['type'] = "success";
            $fecha = new DateTime();
            $timestamp = $fecha->format('Y-m-d H:i:s') ;
            array_push($_SESSION['notifications'],"Rol ".$id." editado correctamente el ".$timestamp);
            header($ruta);
        } else {
            $_SESSION['message'] = "Error al editar el Rol.";
            $_SESSION['type'] = "danger";
            $fecha = new DateTime();
            $timestamp = $fecha->format('Y-m-d H:i:s') ;
            array_push($_SESSION['notifications'],"Error al editar el Rol ".$id." el ".$timestamp);
            header($ruta);
        }
    }






?>