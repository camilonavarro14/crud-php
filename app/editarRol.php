<?php
    require_once "../functions.php";
    session_start();
    $id = $_POST['idRole'];
    $name = $_POST['NomRole'];
    $description = $_POST['DesRole'];

    $updated = updateRol($id,$name,$description);
    
    if ($updated) {
        $_SESSION['message'] = "Rol editado correctamente...";
        $_SESSION['type'] = "success";
        $fecha = new DateTime();
        $timestamp = $fecha->format('Y-m-d H:i:s') ;
        array_push($_SESSION['notifications'],"Rol ".$id." editado correctamente el ".$timestamp);
        header("Location: ../views/adminRoles.php");
    }else{
        $_SESSION['message'] = "Error al editar el Rol.";
        $_SESSION['type'] = "danger";
        $fecha = new DateTime();
        $timestamp = $fecha->format('Y-m-d H:i:s') ;
        array_push($_SESSION['notifications'],"Error al editar el Rol ".$id." el ".$timestamp);
        header("Location: ../views/adminRoles.php");
    }
?>