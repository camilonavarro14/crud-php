<?php
    require_once "../functions.php";
    session_start();
    $id = $_GET['id'];

    $deleted = deleteRol($id);
    
    if ($deleted) {
        $_SESSION['message'] = "Rol eliminado correctamente...";
        $_SESSION['type'] = "danger";
        $fecha = new DateTime();
        $timestamp = $fecha->format('Y-m-d H:i:s') ;
        array_push($_SESSION['notifications'],"Rol ".$id." eliminado correctamente el ".$timestamp);
        header("Location: ../views/adminRoles.php");
    }else{
        $_SESSION['message'] = "Error al eliminar el Rol.";
        $_SESSION['type'] = "danger";
        $fecha = new DateTime();
        $timestamp = $fecha->format('Y-m-d H:i:s') ;
        array_push($_SESSION['notifications'],"Error al eliminar el Rol ".$id." el ".$timestamp);
        header("Location: ../views/adminRoles.php");
    }
?>