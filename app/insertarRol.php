<?php
    require_once "../functions.php";
    session_start();

    $name = $_POST['NomRole'];
    $description = $_POST['DesRole'];
    $created = createRol($name,$description);
    
    if ($created) {
        $_SESSION['message'] = "Rol creado correctamente...";
        $_SESSION['type'] = "success";
        $fecha = new DateTime();
        $timestamp = $fecha->format('Y-m-d H:i:s') ;
        array_push($_SESSION['notifications'],"Rol creado correctamente el ".$timestamp);
        header("Location: ../views/adminRoles.php");
    }else{
        $_SESSION['message'] = "Error al crear el Rol.";
        $_SESSION['type'] = "danger";
        $fecha = new DateTime();
        $timestamp = $fecha->format('Y-m-d H:i:s') ;
        array_push($_SESSION['notifications'],"Error al crear Rol el ".$timestamp);
        header("Location: ../views/adminRoles.php");
    }
?>