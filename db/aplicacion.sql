-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-07-2021 a las 23:59:01
-- Versión del servidor: 10.4.19-MariaDB
-- Versión de PHP: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `aplicacion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `idMenu` int(11) NOT NULL,
  `NomMenu` varchar(45) NOT NULL,
  `DesMenu` varchar(500) DEFAULT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `naturaleza`
--

CREATE TABLE `naturaleza` (
  `IdNaturaleza` int(11) NOT NULL,
  `NomNaturaleza` varchar(50) NOT NULL,
  `CodNaturaleza` varchar(5) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `novedadproducto`
--

CREATE TABLE `novedadproducto` (
  `IdNovedad` int(11) NOT NULL,
  `IdProducto` int(11) NOT NULL,
  `FecNovedad` datetime NOT NULL DEFAULT current_timestamp(),
  `IndTipoNovedad` tinyint(1) NOT NULL DEFAULT 1,
  `Cantidad` int(11) NOT NULL,
  `IdUsuarioR` int(11) NOT NULL,
  `FecRegistro` datetime NOT NULL DEFAULT current_timestamp(),
  `IdUsuarioModifica` int(11) DEFAULT NULL,
  `FecModifica` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `IdProducto` int(11) NOT NULL,
  `NomProducto` varchar(255) NOT NULL,
  `DesProducto` varchar(255) NOT NULL,
  `CodProducto` varchar(20) DEFAULT NULL,
  `Cantidad` int(11) NOT NULL,
  `IdProductoTipo` int(11) NOT NULL,
  `IdFabricante` int(11) NOT NULL,
  `IdTercero` int(11) NOT NULL,
  `IdIvaCompra` varchar(4) DEFAULT NULL,
  `IdIvaVenta` varchar(4) DEFAULT NULL,
  `Valor` float DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `FecRegistro` datetime DEFAULT NULL,
  `IdUsuarioR` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productotipo`
--

CREATE TABLE `productotipo` (
  `IdProductoTipo` int(11) NOT NULL,
  `NomProductoTipo` varchar(255) NOT NULL,
  `DesproductoTipo` varchar(400) DEFAULT NULL,
  `CodProductoTipo` varchar(8) NOT NULL,
  `IndInventario` bit(1) NOT NULL,
  `IndCodLegal` bit(1) NOT NULL,
  `IndLote` bit(1) NOT NULL,
  `IndVenta` bit(1) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profabricante`
--

CREATE TABLE `profabricante` (
  `IdFabricante` int(11) NOT NULL,
  `Nomfabricante` varchar(30) NOT NULL,
  `SigFabricante` varchar(15) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE `role` (
  `idRole` int(11) NOT NULL,
  `NomRole` varchar(45) NOT NULL,
  `DesRole` varchar(45) NOT NULL,
  `estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roleacceso`
--

CREATE TABLE `roleacceso` (
  `idRoleAcceso` int(11) NOT NULL,
  `idMenu` int(11) NOT NULL,
  `idRole` int(11) NOT NULL,
  `estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `idRole` int(11) NOT NULL,
  `NomRole` varchar(45) DEFAULT NULL,
  `DesRole` varchar(45) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`idRole`, `NomRole`, `DesRole`, `estado`) VALUES
(2, 'Admin', 'Admin Uno', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tercero`
--

CREATE TABLE `tercero` (
  `IdTercero` int(11) NOT NULL,
  `IdTipoDoc` int(11) NOT NULL,
  `NumDocumento` varchar(15) NOT NULL,
  `DigVerificacion` int(11) DEFAULT NULL,
  `PriNombre` varchar(50) DEFAULT NULL,
  `SegNombre` varchar(50) DEFAULT NULL,
  `PriApellido` varchar(50) DEFAULT NULL,
  `SegApellido` varchar(50) DEFAULT NULL,
  `OtrNom` varchar(50) DEFAULT NULL,
  `RazonSocial` varchar(50) NOT NULL,
  `IdNaturaleza` int(11) NOT NULL,
  `IdTipoTercero` int(11) NOT NULL,
  `IndAutoretencion` tinyint(1) DEFAULT NULL,
  `IndGContribuyente` tinyint(1) DEFAULT NULL,
  `IndCliente` tinyint(1) DEFAULT NULL,
  `IndEmpleado` tinyint(1) DEFAULT NULL,
  `IndProveedor` tinyint(1) DEFAULT NULL,
  `Estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodocumento`
--

CREATE TABLE `tipodocumento` (
  `IdTipoDoc` int(11) NOT NULL,
  `NomTipoDocu` varchar(15) NOT NULL,
  `CodTipoDocu` varchar(5) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipotercero`
--

CREATE TABLE `tipotercero` (
  `IdTipoTercero` int(11) NOT NULL,
  `NomTipoTercero` varchar(10) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `IdUsuario` int(11) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `NomUsuario` varchar(255) NOT NULL,
  `Contrasena` varchar(255) NOT NULL,
  `UltimoLogin` timestamp NOT NULL DEFAULT current_timestamp(),
  `FechaR` timestamp NOT NULL DEFAULT current_timestamp(),
  `Estado` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`IdUsuario`, `Email`, `NomUsuario`, `Contrasena`, `UltimoLogin`, `FechaR`, `Estado`) VALUES
(1, 'mao.parra@hotmail.com', 'mao.parra', '63a9f0ea7bb98050796b649e85481845', '2021-06-30 07:14:45', '2021-06-30 07:14:45', 1),
(2, 'jullie.mantilla@gmail.com', 'jullie.mantilla', '63a9f0ea7bb98050796b649e85481845', '2021-07-01 04:23:43', '2021-07-01 04:23:43', 1),
(3, 'ing.andreamantilla@gmail.com', 'andrea.mantilla', '63a9f0ea7bb98050796b649e85481845', '2021-07-05 03:25:03', '2021-07-05 03:25:03', 1),
(4, 'mauricio.parra@gmail.com', 'mauricio.parra', '63a9f0ea7bb98050796b649e85481845', '2021-07-05 03:27:50', '2021-07-05 03:27:50', 1),
(5, 'prueba@gmail.com', 'prueba', '63a9f0ea7bb98050796b649e85481845', '2021-07-05 04:41:04', '2021-07-05 04:41:04', 1),
(6, 'prueba2@gmail.com', 'usuario2', '63a9f0ea7bb98050796b649e85481845', '2021-07-05 04:42:04', '2021-07-05 04:42:04', 1),
(7, '', '', '63a9f0ea7bb98050796b649e85481845', '2021-07-06 22:38:49', '2021-07-06 22:38:49', 1),
(8, 'camilonavarro14@gmail.com', 'camilonavarro14@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', '2021-07-08 18:54:12', '2021-07-08 18:54:12', 1),
(9, 'camilonavarro14@outlook.com', 'camilonavarro14', 'd41d8cd98f00b204e9800998ecf8427e', '2021-07-08 18:55:02', '2021-07-08 18:55:02', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariorole`
--

CREATE TABLE `usuariorole` (
  `idUsuarioRole` int(11) NOT NULL,
  `IdUsuario` int(11) NOT NULL,
  `IdRol` int(11) NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`idMenu`);

--
-- Indices de la tabla `naturaleza`
--
ALTER TABLE `naturaleza`
  ADD PRIMARY KEY (`IdNaturaleza`);

--
-- Indices de la tabla `novedadproducto`
--
ALTER TABLE `novedadproducto`
  ADD PRIMARY KEY (`IdNovedad`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`IdProducto`),
  ADD KEY `Producto_ProductoTipo_idx` (`IdProductoTipo`),
  ADD KEY `Producto_Fabricante_idx` (`IdFabricante`),
  ADD KEY `Producto_Tercero_idx` (`IdTercero`),
  ADD KEY `Producto_Usuario_idx` (`IdUsuarioR`);

--
-- Indices de la tabla `productotipo`
--
ALTER TABLE `productotipo`
  ADD PRIMARY KEY (`IdProductoTipo`);

--
-- Indices de la tabla `profabricante`
--
ALTER TABLE `profabricante`
  ADD PRIMARY KEY (`IdFabricante`);

--
-- Indices de la tabla `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`idRole`);

--
-- Indices de la tabla `roleacceso`
--
ALTER TABLE `roleacceso`
  ADD PRIMARY KEY (`idRoleAcceso`),
  ADD KEY `RolAcceso_Rol_idx` (`idRole`),
  ADD KEY `RolAcceso_Menu_idx` (`idMenu`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`idRole`);

--
-- Indices de la tabla `tercero`
--
ALTER TABLE `tercero`
  ADD PRIMARY KEY (`IdTercero`),
  ADD KEY `tercero_TipoDoc_idx` (`IdTipoDoc`),
  ADD KEY `tercero_Naturaleza_idx` (`IdNaturaleza`),
  ADD KEY `tercero_TipoTercero_idx` (`IdTipoTercero`);

--
-- Indices de la tabla `tipodocumento`
--
ALTER TABLE `tipodocumento`
  ADD PRIMARY KEY (`IdTipoDoc`);

--
-- Indices de la tabla `tipotercero`
--
ALTER TABLE `tipotercero`
  ADD PRIMARY KEY (`IdTipoTercero`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`IdUsuario`);

--
-- Indices de la tabla `usuariorole`
--
ALTER TABLE `usuariorole`
  ADD PRIMARY KEY (`idUsuarioRole`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productotipo`
--
ALTER TABLE `productotipo`
  MODIFY `IdProductoTipo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `idRole` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `IdUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `Producto_Fabricante` FOREIGN KEY (`IdFabricante`) REFERENCES `profabricante` (`IdFabricante`),
  ADD CONSTRAINT `Producto_ProductoTipo` FOREIGN KEY (`IdProductoTipo`) REFERENCES `productotipo` (`IdProductoTipo`),
  ADD CONSTRAINT `Producto_Tercero` FOREIGN KEY (`IdTercero`) REFERENCES `tercero` (`IdTercero`),
  ADD CONSTRAINT `Producto_Usuario` FOREIGN KEY (`IdUsuarioR`) REFERENCES `usuario` (`IdUsuario`);

--
-- Filtros para la tabla `roleacceso`
--
ALTER TABLE `roleacceso`
  ADD CONSTRAINT `RolAcceso_Menu` FOREIGN KEY (`idMenu`) REFERENCES `menu` (`idMenu`),
  ADD CONSTRAINT `RolAcceso_Rol` FOREIGN KEY (`idRole`) REFERENCES `role` (`idRole`);

--
-- Filtros para la tabla `tercero`
--
ALTER TABLE `tercero`
  ADD CONSTRAINT `tercero_Naturaleza` FOREIGN KEY (`IdNaturaleza`) REFERENCES `naturaleza` (`IdNaturaleza`),
  ADD CONSTRAINT `tercero_TipoDoc` FOREIGN KEY (`IdTipoDoc`) REFERENCES `tipodocumento` (`IdTipoDoc`),
  ADD CONSTRAINT `tercero_TipoTercero` FOREIGN KEY (`IdTipoTercero`) REFERENCES `tipotercero` (`IdTipoTercero`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
